# Custom Rainmeter
This repository includes my customization files for Rainmeter application on Windows OS.

All files (e.g. Skins, *.ini) should be in	their appropriate place.

To do this automatically You can run [Batch script](./restore2CurrentUser.bat).

```cmd
restore2CurrentUser.bat
```


_2018, Boar Artist_