[Variables]

skinSize=(#SCREENAREAHEIGHT#*0.5)
bottomTextWidth=120


Player=Spotify
alignRight=1

activePlugin=Spotify


; 0 = MUSIC, 1 = SPOTIFY, 2 = WEBNOWPLAYING
MusicSwitch=1
playerController=Title0
currentlySetName=Windows Media Player
currentlySetPlayer=WMP

adaptiveHideActive=0

stowAway=1
stowAwayName=Stow

topText=mArtist
bottomText=mTitle

disableScroll=0

removeExtraFromText=0

thinFont=Metropolis Extra Light
thickFont=Metropolis Medium

mediaTextPositionHor=384
mediaTextAlignment=RightBottom
mediaTextAlignmentPure=Right
interfaceTextAlignment=Left
nowTextPositionHor=441.6
hairlinePositionHor=430.08
topTextPositionHor=418.56
playCtrlPositionHor=474.24
settingsTextPositionHor=470.4
indicatorPositionHor=485.76
indicatorPositionHorPure=372.48
conflictIndicatorPositionHor=464.64
conflictIndicatorPositionHorPure=357.12
