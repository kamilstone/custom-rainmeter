@echo off
 :: ----------------------------------------------------------------
 title Restore Rainmeter configuration files for %username%
 :: version 1.0
 :: date 02-03-2018
 :: author Boar Artist
 :: ----------------------------------------------------------------
 
: variables
set  _appName=Rainmeter
set  _appDataDir=%appdata%\%_appName% 
set  _appUserDocsDir=%userprofile%\Documents\%_appName%

: prepare directory
IF EXIST %_appDataDir% (
	echo Removing already existig files...
	rmdir /S %_appDataDir%
	rmdir /S %_appUserDocsDir%
) ELSE (
	IF EXIST %_appUserDocsDir% (
		echo Removing already existig files...
		rmdir /S %_appUserDocsDir%
	)
)
echo.

: restore files
echo Copying hiden AppData configuration files...
xcopy .\src\windows10\userprofile\AppData\Roaming\%_appName% %_appDataDir% /S /I
echo.

echo Copying user's binary files...
xcopy .\src\windows10\userprofile\Documents\%_appName% %_appUserDocsDir% /S /I
echo.

echo Script finish.
:exiting
pause


